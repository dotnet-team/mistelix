AC_INIT(mistelix, 0.33)
AC_CONFIG_SRCDIR(src/mistelix.cs)

AM_CONFIG_HEADER(config.h)

AM_INIT_AUTOMAKE([tar-ustar])

AM_MAINTAINER_MODE

AC_PROG_INTLTOOL([0.35])

AC_PROG_CC
AC_ISC_POSIX
AC_HEADER_STDC
AM_PROG_LIBTOOL

GNOME_COMPILE_WARNINGS
AM_PATH_GLIB_2_0

dnl --- Check for mono and gmcs

AC_PATH_PROG(MONO, mono)
AC_PATH_PROG(MCS, gmcs)

CS="C#"
if test ! -x "$MCS"; then
	AC_MSG_ERROR([No $CS compiler found])
fi


dnl -- Check for mono pc file

AC_MSG_CHECKING([for mono.pc])
if test -z `$PKG_CONFIG --variable=prefix mono`; then
  AC_MSG_ERROR([missing the mono.pc file, usually found in the mono-devel package])
else
  AC_MSG_RESULT([found])
fi

dnl -- check for various mono DLLs that we need.

needed_dlls="Mono.Posix Mono.Cairo"
for i in $needed_dlls; do
  AC_MSG_CHECKING([for $i.dll])
  if test ! \( -e `$PKG_CONFIG --variable=prefix mono`/lib/mono/2.0/$i.dll -o -e `$PKG_CONFIG --variable=prefix mono`/lib64/mono/2.0//$i.dll \); then
    AC_MSG_ERROR([missing required mono 2.0 DLL: $i.dll])
  else
    AC_MSG_RESULT([found])
  fi
done

dnl --- Required libraries

dnl --- Required Gstreamer libraries for gstreamer plug-in

GSTREAMER_REQUIRED_VERSION=0.10.3
AC_SUBST(GSTREAMER_REQUIRED_VERSION)

PKG_CHECK_MODULES(GST,
	gstreamer-0.10 >= $GSTREAMER_REQUIRED_VERSION
	gstreamer-base-0.10 >= $GSTREAMER_REQUIRED_VERSION
	gstreamer-plugins-base-0.10 >= $GSTREAMER_REQUIRED_VERSION)

AC_SUBST(GST_CFLAGS)
AC_SUBST(GST_LIBS)

# 2.10 is required for Pango / Cairo compatibility
GTKSHARP_REQUIRED=2.10
MONO_REQUIRED=1.1.7

PKG_CHECK_MODULES(MISTELIX_CORE, mono >= $MONO_REQUIRED)

PKG_CHECK_MODULES(MISTELIX,
	gtk-sharp-2.0 >= $GTKSHARP_REQUIRED
	gnome-sharp-2.0 >= $GTKSHARP_REQUIRED
	mono-addins >= 0.3
	mono-addins-setup >= 0.3
	mono-addins-gui >= 0.3)
AC_SUBST(MISTELIX_LIBS)

dnl -- Intl

GETTEXT_PACKAGE=mistelix
AC_SUBST(GETTEXT_PACKAGE)
AC_DEFINE_UNQUOTED(GETTEXT_PACKAGE,"$GETTEXT_PACKAGE", [Gettext package])
AM_GLIB_GNU_GETTEXT

LIBGNOME_DESKOP_SO_MAP=$(basename $(find $($PKG_CONFIG --variable=libdir gnome-desktop-2.0) -maxdepth 1 -regex '.*libgnome-desktop-2.so\.[[0-9]][[0-9]]*$' | sort | tail -n 1))
AC_SUBST(LIBGNOME_DESKOP_SO_MAP)

dnl --- libmistelix requirements

PKG_CHECK_MODULES(LIBMISTELIX,
	glib-2.0
	gstreamer-0.10 >= $GSTREAMER_REQUIRED_VERSION
	gstreamer-base-0.10 >= $GSTREAMER_REQUIRED_VERSION
	gstreamer-plugins-base-0.10 >= $GSTREAMER_REQUIRED_VERSION)

LIBMISTELIX_LIBS="$LIBMISTELIX_LIBS -lgstvideo-0.10 -lgstinterfaces-0.10 -lgstcdda-0.10"

AC_SUBST(LIBMISTELIX_CFLAGS)
AC_SUBST(LIBMISTELIX_LIBS)

dnl --- Prologue

AC_SUBST(CFLAGS)
AC_SUBST(CPPFLAGS)
AC_SUBST(LDFLAGS)

CSC=gmcs
AC_SUBST(CSC)

expanded_libdir=`( case $prefix in NONE) prefix=$ac_default_prefix ;; *) ;; esac
		   case $exec_prefix in NONE) exec_prefix=$prefix ;; *) ;; esac
		   eval echo $libdir )`
AC_SUBST(expanded_libdir)

AC_CONFIG_FILES([src/mistelix],[chmod +x src/mistelix])

GNOME_ICON_THEME_PREFIX=`$PKG_CONFIG --variable=prefix gnome-icon-theme`
AC_SUBST(GNOME_ICON_THEME_PREFIX)



AC_OUTPUT([
Makefile
mistelix.pc
data/Makefile
data/mistelix.exe.config
src/Defines.cs
src/Makefile
gstreamer/Makefile
po/Makefile.in
libmistelix/Makefile
extensions/Makefile
extensions/SlideTransitions/Makefile
extensions/SlideTransitions/BarWipe/Makefile
extensions/SlideTransitions/BarnDoorWipe/Makefile
extensions/SlideTransitions/Fade/Makefile
extensions/SlideTransitions/StarWipe/Makefile
extensions/Effects/Makefile
extensions/Effects/Grayscale/Makefile
extensions/Effects/SepiaTone/Makefile
])
