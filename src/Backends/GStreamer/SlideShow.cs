//
// Copyright (C) 2009 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using System.Runtime.InteropServices;

using Mistelix.DataModel;

namespace Mistelix.Backends.GStreamer
{
	//
	// GStreamer slideshow functions
	// 
	public static class SlideShow
	{
		[DllImport ("libmistelix")]
		static extern int mistelix_slideshow_createstream (string filename, uint type, uint weight, uint height, uint framessec, uint totalframes);

		[DllImport ("libmistelix")]
		static extern int mistelix_slideshow_add_image (IntPtr bytes, uint len);

		[DllImport ("libmistelix")]
		static extern int mistelix_slideshow_add_imagefixed (IntPtr bytes, uint len, uint frames);

		[DllImport ("libmistelix")]
		static extern void mistelix_slideshow_add_audio (string filename);

		[DllImport ("libmistelix")]
		static extern void mistelix_slideshow_close ();

		static SlideShow ()
		{

		}

		static public int CreateStream (string filename, ProjectType type, uint weight, uint height, uint framessec, uint totalframes)
		{
			return mistelix_slideshow_createstream (filename, (uint) type, weight, height, framessec, totalframes);
		}
		
		static public int AddImage (SlideImage image)
		{
			IntPtr pixels = IntPtr.Zero;
			int rslt = -1;
	
			try
			{
				if (image.Channels != 3)
					throw new InvalidOperationException (String.Format ("mistelixvideosrc expects images in 24 bits (3 channels) not {0}", image.Channels));

				pixels = Marshal.AllocHGlobal (image.Pixels.Length);
				Marshal.Copy (image.Pixels, 0, pixels, image.Pixels.Length);

				rslt = mistelix_slideshow_add_image (pixels, (uint) image.Pixels.Length);
				return rslt;
			}

			finally
			{
			 	Marshal.FreeHGlobal (pixels);
			}
		}

		static public int AddImageFixed (SlideImage image, uint frames)
		{
			IntPtr pixels = IntPtr.Zero;
			int rslt = -1;

			try
			{
				if (image.Channels != 3)
					throw new InvalidOperationException (String.Format ("mistelixvideosrc expects images in 24 bits (3 channels) not {0}", image.Channels));

				pixels = Marshal.AllocHGlobal (image.Pixels.Length);
				Marshal.Copy (image.Pixels, 0, pixels, image.Pixels.Length);
	
				rslt = mistelix_slideshow_add_imagefixed (pixels, (uint) image.Pixels.Length, frames);
				return rslt;
			}

			finally
			{
			 	Marshal.FreeHGlobal (pixels);
			}
		}

		static public void AddAudio (string file)
		{
			try
			{
				mistelix_slideshow_add_audio (file);
			}

			finally
			{

			}
		}

		static public void Close ()
		{
			mistelix_slideshow_close ();
		}
	}
}
