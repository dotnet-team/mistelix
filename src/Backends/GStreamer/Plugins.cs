//
// Copyright (C) 2009 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using Mistelix.Core;

namespace Mistelix.Backends.GStreamer
{
	//
	// GStreamer plugin enumeration functions
	// 
	public static class Plugins
	{
		[DllImport ("libmistelix")]
		static extern uint mistelix_get_plugins_count ();

		[DllImport ("libmistelix")]
		static extern IntPtr mistelix_get_plugins (IntPtr [] plugins);
		
		static Plugins ()
		{

		}

		static public List <string> GetList ()
		{
			uint count;
			IntPtr[] list;
			string codec;
			List <string> codecs = new List <string> ();

			count = mistelix_get_plugins_count ();
			Logger.Debug ("Plugin.GetList. Gstreamer pluggins: {0}", count);
			list = new IntPtr [count];
			mistelix_get_plugins (list);

			for (int i = 0; i < count; i++)
			{
				codec = Marshal.PtrToStringAuto (list[i]);
				codecs.Add (codec);
				Marshal.FreeHGlobal (list[i]);
			}
			return codecs;
		}
	}
}
