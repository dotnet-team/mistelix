//
// Copyright (C) 2009 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using System.Runtime.InteropServices;
using Gdk;

namespace Mistelix.Backends.GStreamer
{
	//
	// GStreamer video thumbnailing functions
	// 
	public static class Thumbnail
	{
		[DllImport ("libmistelix")]	
		static extern void mistelix_video_screenshot (string filein, int second, out IntPtr pixbuf);
	
		
		static Thumbnail ()
		{

		}

		static public Gdk.Pixbuf VideoScreenshot (string file, int second)
		{
			Gdk.Pixbuf thumbnail;
			IntPtr pix;

			mistelix_video_screenshot (file, second, out pix);

			if (pix == IntPtr.Zero)
				throw new InvalidOperationException ("No screenshot taken");

			thumbnail = new Gdk.Pixbuf (pix);
			return thumbnail;
		}
	}
}
