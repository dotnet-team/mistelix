//
// Copyright (C) 2009 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using System.Runtime.InteropServices;
using Gdk;

using Mistelix.Core;

namespace Mistelix.Backends.ThumbnailCache
{
	//
	// Returns the appropiate class for managing Thumbnails
	// 
	public static class Factory
	{
		static Provider provider;

		static Factory ()
		{
			// See: http://www.mono-project.com/FAQ:_Technical
			int os = (int) System.Environment.OSVersion.Platform;
			if (os == 4 || os == 6 || os == 128) {
				provider = new Gnome ();
				if (provider.Installed == false)
					provider = null;
			}

			if (provider == null)
				provider = new None ();
	
			Logger.Debug ("Factory.Factory. Provider {0}", provider);
		}
		
		public static Provider Provider {
			get {return provider; }
		}
	}
}
