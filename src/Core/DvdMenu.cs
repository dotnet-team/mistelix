//
// Copyright (C) 2008-2009 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using System.IO;
using Gdk;
using Mono.Addins;
using System.Collections.Generic;
using System.ComponentModel;

using Mistelix.Transitions;
using Mistelix.DataModel;
using Mistelix.Widgets;

namespace Mistelix.Core
{
	// Generates MPEG for DVD menu. Designed for a single menu
	public class DvdMenu : IDisposable
	{
		Project project;
		Gdk.Pixbuf background;
		bool asynchronous;
		BackgroundWorker thumbnailing;

		public EventHandler CompletedThumbnails;

		public DvdMenu (Project project)
		{
			this.project = project;
			UpdateTheme ();
		}

		~DvdMenu ()
		{
			Dispose (false);
		}

		public bool Asynchronous {
			get { return asynchronous; }
			set {
				if (value == true && CompletedThumbnails == null)
					throw new InvalidOperationException ("You must set the CompletedThumbnail event");

				asynchronous = value;

				if (value == true) {
					thumbnailing = new BackgroundWorker ();
					thumbnailing.WorkerSupportsCancellation = true;
					thumbnailing.DoWork += new DoWorkEventHandler (DoWork);
				}
			}
		}

		public void Dispose ()
		{
			Dispose (true);
			System.GC.SuppressFinalize (this);
		}

		protected virtual void Dispose (bool disposing)
		{	
			Logger.Debug ("DvdMenu.Disposing");
			if (background != null)
				background.Dispose ();
		}

		public string GenerateMPEG (/*, ProgressEventHandler progress*/)
		{
			SlideShow sw;

			Logger.Debug ("DvdMenu.GenerateMPEG");
	
			Save (project.FileToFullPath (Defines.MAIN_MENU_FILE_PNG));

			string outfile;
			sw = new SlideShow ();
			sw.filename = Defines.MAIN_MENU_FILE_SRC;
			sw.images.Add (new SlideImage (project.FileToFullPath (Defines.MAIN_MENU_FILE_PNG), string.Empty, 5, TransitionManager.None.Name));
			outfile = sw.Generate (project, null);
		
			if (Mistelix.Debugging == false)
				File.Delete (project.FileToFullPath (Defines.MAIN_MENU_FILE_PNG));

			return outfile;
		}

		public void UpdateTheme ()
		{	
			string name;
			Theme theme = null;
			Gdk.Pixbuf im = null;

			if (project == null)
				return;

			name = project.Details.ThemeName;

			if (name != null)
				theme = ThemeManager.FromName (name);

			if (background != null)
				background.Dispose ();

			foreach (Button button in project.Buttons)
				button.InvalidateThumbnail ();
	
			if (theme == null) {
				background = null;
				return;
			}

			try {
				if (string.IsNullOrEmpty (project.Details.CustomMenuBackground) == false)
					im = new Gdk.Pixbuf (project.Details.CustomMenuBackground);

			}
			catch {
				Logger.Error (String.Format ("DvdMenu.UpdateTheme. Error loading custom background {0}", project.Details.CustomMenuBackground));
			}
			
			try {
				if (im == null)
					im = new Gdk.Pixbuf (Path.Combine (Defines.DATA_DIR, theme.MenuBackground));
			}

			catch {
				Logger.Error (String.Format ("DvdMenu.UpdateTheme. Error loading menu background {0}", Path.Combine (Defines.DATA_DIR, theme.MenuBackground)));
			}

			background = im.ScaleSimple (project.Details.Width, project.Details.Height, InterpType.Nearest);
			im.Dispose ();
		}
	
		public void Draw (Cairo.Context cr, Core.Button moving_button)
		{
			bool fire_thread = false;

			// See: http://kapo-cpp.blogspot.com/2008/01/drawing-pixbuf-to-cairo-context-2.html

			if (background != null) {
				Gdk.CairoHelper.SetSourcePixbuf (cr, background, 0, 0);
				cr.Paint ();
			}

			if (project == null)
				return;

			// Draw buttons
			foreach (Button button in project.Buttons)
			{
				if (moving_button != null && moving_button.LinkedId == button.LinkedId) {
					if (DrawButton (cr, button) == true)
						fire_thread = true;

					Utils.DrawSelectionBox (cr, moving_button.X, moving_button.Y, button.DrawingBoxWidth, button.DrawingBoxHeight);
				}
				else {
					if (DrawButton (cr, button) == true)
						fire_thread = true;
				}
			}

			if (fire_thread == true) {
				if (thumbnailing.IsBusy == false)
					thumbnailing.RunWorkerAsync (this);
			}
		}
		
		// return value indicates if the thumbnail of the button was not painted and we need to create them after
		bool DrawButton (Cairo.Context cr, Button button)
		{
			if (asynchronous == false || 
				(asynchronous == true && button.IsThumbnailCreated)) {
				// In the button object the image is already loaded and cached
				button.Draw (cr, project);
				return false;
			}
			return true;
		}

		public void Save (string file)
		{
			Cairo.ImageSurface s = new Cairo.ImageSurface (Cairo.Format.Rgb24, project.Details.Width, project.Details.Height);
			Cairo.Context cr = new Cairo.Context (s);
			bool previous = asynchronous;

			asynchronous = false;
			Draw (cr, null);
			asynchronous = previous;

			s.WriteToPng (file);
			((IDisposable)cr).Dispose ();
			s.Destroy ();
		}

		void DoWork (object sender, DoWorkEventArgs e)
		{
			foreach (Button button in project.Buttons)
				button.LoadThumbnail (project);

			if (CompletedThumbnails != null)
				CompletedThumbnails (this, EventArgs.Empty);
		}
	}
}
