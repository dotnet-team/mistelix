//
// Copyright (C) 2009 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using System.Collections.Generic;
using Mono.Unix;
using System.Diagnostics;

using Mistelix.Transitions;
using Mistelix.DataModel;
using Mistelix.Backends.GStreamer;

namespace Mistelix.Core
{
	public class Dependencies
	{
		enum Codecs
		{
			MPEG2VIDEO,
			FFMUX_DVD,
			MISTELIXVIDEOSRC,
			THEORAENC,
			OGGMUX,
			VORBISENC,
			FLUMP3DEC,
			ASFDEMUX,
		};

		public struct Dependency
		{
			string component;
			bool available;
			string action;

			public Dependency (string component, bool available, string action)
			{
				this.component = component;
				this.available = available;

				if (available)
					this.action = Catalog.GetString ("No action required");
				else
					this.action = action;
			}

			public string Component {
				get { return component; } 
			}

			public bool Available {
				get { return available; } 
			}

			public string Action {
				get { return action; } 
			}
		}

		List <Dependency> DependencyList;
		bool status_checked;
		bool dvd_support, theora_support, mp3_support;

		public Dependencies ()
		{
			DependencyList = new List <Dependency> ();
		}

		public bool DvdSupport {
			get {
				Check ();
				return dvd_support;
			}
		}

		public string CapabilitiesSummary {
			get {
				Check ();

				if (theora_support && dvd_support)
					return Catalog.GetString ("Your system supports DVD and Theora authoring.");

				if (dvd_support)
					return Catalog.GetString ("Your system only supports DVD authoring.");

				if (theora_support)
					return Catalog.GetString ("Your system only supports Theora authoring.");

				return Catalog.GetString ("Your system has no support for DVD or Theora authoring. Please, execute the suggested actions to enhance your system support for Mistelix.");
			}
		}

		public bool TheoraSupport {
			get {
				Check ();
				return theora_support;
			}
		}

		public bool MP3Support {
			get {
				Check ();
				return mp3_support;
			}
		}

		public List <Dependency> Status {
			get {
				Check ();
				return DependencyList;
			}
		}

		void Check ()
		{
			if (status_checked == true)
				return;

			status_checked = true;
			List <string> codecs = Plugins.GetList ();
			string [] used =  {"ffenc_mpeg2video", "ffmux_dvd", "mistelixvideosrc", "theoraenc", "oggmux", "vorbisdec", "flump3dec", "asfdemux"};
			bool [] founds = new bool [used.Length];

			// TODO: Better a hash table
			foreach (string codec in codecs)
			{
				for (int i = 0; i < used.Length; i++)
				{
					if (codec.Equals (used[i])) 
					{
						founds[i] = true;
						Logger.Info ("GStreamer codec found: " + used[i]);
						break;
					}
				}
			}

			DependencyList.Add (new Dependency (Catalog.GetString ("Mistelix GStreamer plugin"), founds [(int) Codecs.MISTELIXVIDEOSRC],
				Catalog.GetString ("Missing mistelixvideosrc plug-in. This GStreamer plug-in is provided as part of Mistelix application. Mistelix is not installed correctly.")));

			CheckTheora (used, founds);
			CheckFmpeg (used, founds);
	
			DependencyList.Add (new Dependency (Catalog.GetString ("MP3 audio decoder"), founds [(int) Codecs.FLUMP3DEC],
				Catalog.GetString ("Missing MP3 audio decoder. You need to install Fluendo MP3 decoder GStreamer plugin to enable MP3 import audio capabilities.")));

			DependencyList.Add (new Dependency (Catalog.GetString ("GStreamer Ugly Plugins"), founds [(int) Codecs.ASFDEMUX],
				Catalog.GetString ("Missing video decoders. Without this package Mistelix cannot provide video conversion facilities from popular formats. You need to install GStreamer Ugly Plugins package for better video import support.")));

			// Check for applications
			bool dvdauthor, spumux;
	
			dvdauthor = CheckApp ("dvdauthor");
			spumux = CheckApp ("spumux");

			DependencyList.Add (new Dependency (Catalog.GetString ("dvdauthor package"), (dvdauthor && spumux == true),
				Catalog.GetString ("Install dvdauthor package from your Linux distribution application manager.")));

			// DVD Authoring support
			if (founds [(int) Codecs.FFMUX_DVD] && 
				founds [(int) Codecs.MPEG2VIDEO] && 
				founds [(int) Codecs.MISTELIXVIDEOSRC]
				&& spumux 
				&& dvdauthor) 
			{
				dvd_support = true;
			}


			// Theora support
			if (founds [(int) Codecs.THEORAENC] && 
				founds [(int) Codecs.OGGMUX] && 
				founds [(int) Codecs.MISTELIXVIDEOSRC])
			{
				theora_support = true;
			}
	
			mp3_support = founds [(int) Codecs.FLUMP3DEC];
		}

		void CheckFmpeg (string [] used, bool [] founds)
		{
			string str, codec_name;
			bool all, none;
	
			str = Catalog.GetString ("Missing {0}. You need to install GStreamer FFmpeg Plug-ins package.");

			all = founds [(int) Codecs.MPEG2VIDEO] && founds [(int) Codecs.FFMUX_DVD];
			none = founds [(int) Codecs.MPEG2VIDEO] == false && founds [(int) Codecs.FFMUX_DVD] == false;

			// If all the codecs are present or missing give a single message
			if (all || none) {
				codec_name = Catalog.GetString ("FFmpeg codecs");
				DependencyList.Add (new Dependency (codec_name, all == true, String.Format (str, codec_name)));
				return;
			}

			DependencyList.Add (new Dependency (used [(int) Codecs.MPEG2VIDEO], founds [(int) Codecs.MPEG2VIDEO],
				String.Format (Catalog.GetString ("Missing ffmpeg2video codec. Many distributions do not expose MPEG2 encoding capabilities in their GStreamer FFmpeg Plug-ins package. Visit '{0}' for instructions on how to fix this."),
				Defines.PROJECT_URL)));

			DependencyList.Add (new Dependency (used [(int) Codecs.FFMUX_DVD], founds [(int) Codecs.FFMUX_DVD],
				Catalog.GetString ("Missing ffmux_dvd muxer. Probably you have an old version of GStreamer plug-in that does not exposes the DVD muxer.")));
		}

		void CheckTheora (string [] used, bool [] founds)
		{
			string str, codec_name;
			bool all, none;
	
			str = Catalog.GetString ("Missing {0}. You need to install GStreamer Base Plugins package.");

			all = founds [(int) Codecs.THEORAENC] && founds [(int) Codecs.OGGMUX] && founds [(int) Codecs.VORBISENC];
			none = founds [(int) Codecs.THEORAENC] == false && founds [(int) Codecs.OGGMUX] == false && founds [(int) Codecs.VORBISENC] == false;

			// If all the codecs are present or missing give a single message
			if (all || none) {
				codec_name = Catalog.GetString ("Theora/Vorbis codecs");
				DependencyList.Add (new Dependency (codec_name, all == true, String.Format (str, codec_name)));
				return;
			}
		
			// Go into detail
			codec_name = Catalog.GetString ("Theora video codec");
			DependencyList.Add (new Dependency (used [(int) Codecs.THEORAENC], founds [(int) Codecs.THEORAENC],
				String.Format (str, codec_name)));

			codec_name = Catalog.GetString ("ogg muxer");
			DependencyList.Add (new Dependency (used [(int) Codecs.OGGMUX], founds [(int) Codecs.OGGMUX],
				String.Format (str, codec_name)));

			codec_name = Catalog.GetString ("Vorbis audio encoder");
			DependencyList.Add (new Dependency (used [(int) Codecs.VORBISENC], founds [(int) Codecs.VORBISENC],
				String.Format (str, codec_name)));
		}


		bool CheckApp (string application)
		{
			bool app_exists = false;

			try {
				Process process = new Process ();
				process.StartInfo.FileName = application;
				process.StartInfo.UseShellExecute = false; 
				process.StartInfo.RedirectStandardOutput = true;
				process.StartInfo.RedirectStandardError = true;
				process.Start ();
				app_exists = true;
			}
			catch (Exception) {}

			return app_exists;
		}
	}
}
