//
// Copyright (C) 2008-2009 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using Gtk;
using Cairo;

namespace Mistelix.Widgets
{
	public class RequestThumbnailEventArgs: EventArgs
	{
		Gtk.TreeIter iter;

		public RequestThumbnailEventArgs (Gtk.TreeIter iter)
		{
			this.iter = iter;
		}

		public Gtk.TreeIter Iter {
			get { return iter; }
		}
	}

	public delegate void RequestThumbnailEventHandler (object sender, RequestThumbnailEventArgs e);

	// Renders a Cairo Image into a tree Cell
	public class CairoImageCellRenderer : CellRenderer 
	{
		int width, height;
		Cairo.ImageSurface surface, default_surface;
		Gtk.TreeIter iter;
		RequestThumbnailEventHandler request_thumbnail;
		
		public CairoImageCellRenderer (int width, int height, Cairo.ImageSurface default_surface, RequestThumbnailEventHandler request_thumbnail)
		{
			this.width = width;
			this.height = height;
			this.default_surface = default_surface;
			this.request_thumbnail = request_thumbnail;
		}

		public Gtk.TreeIter Iter {
			get { return iter; }
			set { iter = value; }
		}

		public override void GetSize (Widget widget, ref Gdk.Rectangle cell_area, out int x_offset, out int y_offset, out int w, out int h)
		{
			x_offset = 0;
			y_offset = 0;
			w = width + ((int) Xpad) * 2;
			h = height + ((int) Ypad) * 2;
		}

		protected override void Render (Gdk.Drawable window, Widget widget, Gdk.Rectangle background, Gdk.Rectangle cell, Gdk.Rectangle expose, CellRendererState flags)
		{
			Cairo.Context cr = Gdk.CairoHelper.Create (window);
			TreeView parent = (TreeView) widget;

			surface = (DataImageSurface) parent.Model.GetValue (iter, SlideShowImageView.COL_CAIROIMAGE);

			if (surface == null)
				request_thumbnail (this, new RequestThumbnailEventArgs (iter));

			cr.Rectangle (cell.X, cell.Y, cell.Width, cell.Height);
			cr.SetSourceSurface (surface != null ? surface : default_surface, cell.X, cell.Y);
			cr.Paint ();
			(cr as System.IDisposable).Dispose ();
		}
	}
}
