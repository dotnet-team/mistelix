//
// Copyright (C) 2009 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using System.Collections;
using Gdk;

using Mistelix.Core;

namespace Mistelix.DataModel
{
	// Every project visible element can have several thumbnails that can represent it
	// This collection is the base to hold these representations
	public class ThumbnailCollection : IEnumerable
	{
		public class ItemTask : Task
		{
			Pixbuf pixbuf;
			int thumbnail_idx;

			public ItemTask (object obj) : base (obj)
			{

			}
			
			public Pixbuf Pixbuf {
				get {return pixbuf; }
				set {pixbuf = value;}
			}

			public int ThumbnailIndex {
				get {return thumbnail_idx; }
				set {thumbnail_idx = value;}
			}
		}

		TaskDispatcher dispatcher;

		public ThumbnailCollection ()
		{
			dispatcher = new TaskDispatcher ();
		}

		public TaskDispatcher Dispatcher {
			get { return dispatcher; }
		}

		public void AddItem (ItemTask item)
		{
			dispatcher.AddTask (item);
		}

		public IEnumerator GetEnumerator ()
		{
			return dispatcher.Tasks.GetEnumerator ();
		}
	}
}
