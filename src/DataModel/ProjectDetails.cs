//
// Copyright (C) 2008-2009 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using System.Xml.Serialization;

using Mistelix.Core;

namespace Mistelix.DataModel
{	
	public enum VideoFormat
	{
		PAL,
		NTSC
	}

	public enum ProjectType
	{
		Slideshows,
		DVD
	}

	// Contains the details of the project
	public class ProjectDetails 
	{
		string filename;
		string name;
	 	VideoFormat format;
		AspectRatio aspect;
		int width;
		int height;
		string theme;
		string output_dir;
		int next_id;
		ProjectType type;
		string buttons_fontname;
		Cairo.Color buttons_back_color;
		Cairo.Color buttons_fore_color;
		string slideshows_fontname;
		Cairo.Color slideshows_back_color;
		Cairo.Color slideshows_fore_color;
		int button_size;
		string custom_background;

		public ProjectDetails () 
		{
			buttons_back_color = new Cairo.Color (44d / 255d, 56d / 255d, 119d / 255d, 0.5);
			buttons_fore_color = new Cairo.Color (1, 1, 1, 1);
			buttons_fontname = "sans 12";

			slideshows_back_color = new Cairo.Color (44d / 255d, 56d / 255d, 119d / 255d, 0.5);
			slideshows_fore_color = new Cairo.Color (1, 1, 1, 1);
			slideshows_fontname = "sans 12";
		}

		public void IncreaseNextID () 
		{ 
			next_id++; 
		}

		[XmlElementAttribute ("type", DataType="int")]
		public ProjectType Type {
			get { return type;}
			set { type = value;}
		}

		[XmlElementAttribute ("format", DataType="int")]
		public VideoFormat Format {
			get { return format;}
			set { format = value;}
		}

		[XmlElementAttribute ("aspect_ratio", DataType="int")]
		public AspectRatio AspectRatio {
			get { return aspect;}
			set { aspect = value;}
		}

		[XmlElementAttribute ("name")]		
		public string Name {
			get { return name;}
			set { name = value;}
		}

		[System.Xml.Serialization.XmlIgnoreAttribute]
		public string Filename {
			get { return filename;}
			set { filename = value;}
		}

		[XmlElementAttribute ("width")]
		public int Width {
			get { return width;}
			set { width = value;}
		}

		[XmlElementAttribute ("height")]
		public int Height {
			get { return height;}
			set { height = value;}
		}

		[XmlElementAttribute ("theme")]
		public string ThemeName {
			get { return theme;}
			set { theme = value;}
		}

		[XmlElementAttribute ("output_dir")]
		public string OutputDir {
			get { return output_dir;}
			set { output_dir = value;}
		}

		[XmlElementAttribute ("buttons_fontname")]
		public string ButtonsFontName {
			get { return buttons_fontname;}
			set { buttons_fontname = value;}
		}

		[XmlElementAttribute ("buttons_back_color")]
		public Cairo.Color ButtonsBackColor {
			get { return buttons_back_color;}
			set { buttons_back_color = value;}
		}

		[XmlElementAttribute ("buttons_fore_color")]
		public Cairo.Color ButtonsForeColor {
			get { return buttons_fore_color;}
			set { buttons_fore_color = value;}
		}

		[XmlElementAttribute ("slideshows_fontname")]
		public string SlideshowsFontName {
			get { return slideshows_fontname;}
			set { slideshows_fontname = value;}
		}

		[XmlElementAttribute ("slideshows_back_color")]
		public Cairo.Color SlideshowsBackColor {
			get { return slideshows_back_color;}
			set { slideshows_back_color = value;}
		}

		[XmlElementAttribute ("slideshows_fore_color")]
		public Cairo.Color SlideshowsForeColor {
			get { return slideshows_fore_color;}
			set { slideshows_fore_color = value;}
		}

		[XmlElementAttribute ("button_thumbnailsize")]
		public int ButtonThumbnailSize {
			get { return button_size;}
			set { button_size = value;}
		}

		[XmlElementAttribute ("custom_menu_background")]
		public string CustomMenuBackground {
			get { return custom_background;}
			set { custom_background = value;}
		}

		[XmlElementAttribute ("next_id")]
		public int NextID {
			get { return next_id;}
			set { next_id = value;}
		}

		public Theme Theme {
			get {
				if (theme == null)
					return null;

				return ThemeManager.FromName (theme);
			}
		}

		public void SetDvdResolution ()
		{
			if (format == VideoFormat.PAL) {
				Width = 720;
				Height = 576;
			} else {
				Width = 720;
				Height = 480;
			}
		}

		public void SetResolution (int width, int height)
		{
			Width = width;
			Height = height;
		}

		public int FramesPerSecond {
			get {
				if (format == VideoFormat.PAL)
					return 25;
		
				return 30; // VideoFormat.NFTS
			}
		}
	}
}
