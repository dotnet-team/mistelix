//
// Copyright (C) 2008-2009 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//


using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Mistelix.DataModel
{
	public class Theme
	{
		string name;
		string menu_background;
		string button_select;	// select in Spumux terminology
		string button_highlight;
		string author;

		public Theme ()
		{
	
		}

		[XmlElementAttribute ("name")]
		public string Name {
			get { return name; }
			set { name = value; }
		}

		[XmlElementAttribute ("author")]
		public string Author {
			get { return author; }
			set { author = value; }
		}

		[XmlElementAttribute ("menu_background")]
		public string MenuBackground {
			get { return menu_background; }
			set { menu_background = value; }
		}

		[XmlElementAttribute ("button_select")]
		public string ButtonSelect {
			get { return button_select; }
			set { button_select = value; }
		}

		[XmlElementAttribute ("button_highlight")]
		public string ButtonHighlight {
			get { return button_highlight; }
			set { button_highlight = value; }
		}
	}
}
