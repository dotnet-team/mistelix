//
// Copyright (C) 2009 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;
using Mono.Unix;

using System.Xml.Schema;

namespace Mistelix.DataModel
{
	// Contains a RecentFile element that is a part of recent list of files accessed
	public struct RecentFile : IXmlSerializable
	{
		static readonly DateTime UnixStartTime = new DateTime (1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
		static readonly string mistelix_mime = "application/mistelix";

		public string filename;
		public string mimetype;
		public DateTime timestamp;

		public RecentFile (string filename)
		{
			this.filename = filename;
			mimetype = mistelix_mime;
			timestamp = DateTime.UtcNow;
		}

		public string TimeSinceEdited {
			get {
				TimeSpan sincelast = DateTime.UtcNow - timestamp;

				if (sincelast.Days >= 1)
					return String.Format (Catalog.GetPluralString ("{0} day", "{0} days", sincelast.Days), sincelast.Days);
				if (sincelast.Hours >= 1)
					return String.Format (Catalog.GetPluralString ("{0} hour", "{0} hours", sincelast.Hours), sincelast.Hours);
				if (sincelast.Minutes > 0)
					return String.Format (Catalog.GetPluralString ("{0} minute", "{0} minutes", sincelast.Minutes), sincelast.Minutes);
			
				return Catalog.GetString ("Less than a minute");
			}
		}

		public void UpdateTimeStamp ()
		{
			timestamp = DateTime.UtcNow;
		}

		public void WriteXml (XmlWriter writer)
		{
			writer.WriteElementString ("filename", filename);
			writer.WriteElementString ("mimetype", mimetype);
			writer.WriteElementString ("timestamp", ToUnixTime (timestamp).ToString ());
		}

		public void ReadXml (XmlReader reader)
		{
			mimetype = mistelix_mime;

			while (reader.Read ()) 
			{
				if (reader.NodeType != XmlNodeType.Element)
					continue;

				switch (reader.LocalName) {
				case "filename":
					filename = reader.ReadElementString ();
					break;
				case "timestamp": {
					int time = reader.ReadElementContentAsInt ();
					timestamp = ToDateTime (time);
					break;
					}
				default:
					break;
				}
			}
		}
		
		public XmlSchema GetSchema ()
		{
			  return null;
		}

		static int ToUnixTime (DateTime time)
		{
			return (int)(time - UnixStartTime).TotalSeconds;
		}

		static DateTime ToDateTime (int unixTime)
		{
			return UnixStartTime.AddSeconds (unixTime);
		}

		public override string ToString  ()
		{
			return "Filename: " + filename + " timestamp " + timestamp;
		}
	}
}
