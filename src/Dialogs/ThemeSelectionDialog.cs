//
// Copyright (C) 2008-2010 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
using System;
using Gtk;
using Gdk;
using Mono.Unix;
using System.Collections;
using System.IO;

using Mistelix.Core;
using Mistelix.DataModel;
using Mistelix.Widgets;
using Mistelix.Backends;

namespace Mistelix.Dialogs
{	
	// Browse themes dialog
	public class ThemeSelectionDialog : BuilderDialog
	{
		public class MenuBackgroundPreview : DrawingArea
		{
			Theme theme;
			Gdk.Pixbuf background;
			int width, height;
			bool cached_image;
			string custom_menubackground;

			public MenuBackgroundPreview ()
			{
			}

			public Theme Theme {
				set { 
					theme = value;
					
					if (string.IsNullOrEmpty (custom_menubackground) == false)
						return;

					cached_image = false;
					QueueDraw ();
				}
			}

			public string CustomMenuBackground {
				set { 
					custom_menubackground = value;
					cached_image = false;
					QueueDraw ();
				}
			}

			protected override bool OnExposeEvent (Gdk.EventExpose args)
			{
				if (!IsRealized)
					return false;

				int w, h;
				Gdk.Pixbuf im = null;
				Cairo.Context cr = Gdk.CairoHelper.Create (args.Window);
				args.Window.GetSize (out w, out h);

				if (width != w || height != h)
					cached_image = false;

				if (cached_image == false) {
					if (background != null) {
						background.Dispose ();
						background = null;
					}

					try {
						if (string.IsNullOrEmpty (custom_menubackground) == false)
							im = new Gdk.Pixbuf (custom_menubackground);
					}
					catch {
						Logger.Error ("MenuBackgroundPreview.OnExposeEvent. Error loading {0}", custom_menubackground);
					}
			
					try {
						if (im == null)
							im = new Gdk.Pixbuf (System.IO.Path.Combine (Defines.DATA_DIR, theme.MenuBackground));

						background = im.ScaleSimple (w, h, InterpType.Nearest);
						im.Dispose ();
					}
					catch { }

					if (background != null) { // Cached for a certain window size
						cached_image = true;
						width = w;
						height = h;
					}
				}
			
				if (background != null) {
					Gdk.CairoHelper.SetSourcePixbuf (cr, background, 0, 0);
					cr.Paint ();
				}

				((IDisposable)cr).Dispose ();
	   			return base.OnExposeEvent(args);
			}
		}
		
		public class ButtonPreview : DrawingArea
		{
			Project project;
			string image_file;
			bool cached_image;
			int width, height;
			Spumux spumux;
			Cairo.ImageSurface image;

			public ButtonPreview (Project project)
			{
				this.project = project;
				spumux = new Spumux (project);
			}

			public string ImageFile {
				set { 
					image_file = value;					
					cached_image = false;
					QueueDraw ();
				}
			}

			protected override bool OnExposeEvent (Gdk.EventExpose args)
			{
				if (!IsRealized)
					return false;

				int w, h;
				Cairo.Context cr = Gdk.CairoHelper.Create (args.Window);
				args.Window.GetSize (out w, out h);

				if (width != w || height != h)
					cached_image = false;

				if (cached_image == false) {

					if (image != null)
						((IDisposable)image).Dispose ();

					image = new Cairo.ImageSurface (Cairo.Format.ARGB32, project.Details.Width, project.Details.Height);
					Cairo.Context chight = new Cairo.Context (image);

					spumux.DrawImageFromFile (chight,  image_file, 0,  h / 4, w / 2, h / 2);
					cached_image = true;
					((IDisposable)chight).Dispose ();
				}
			
				if (image != null) {
					cr.SetSourceSurface (image, 0, 0);
					cr.Paint ();
					cached_image = true;
					width = w;
					height = h;
				}

				((IDisposable)cr).Dispose ();
	   			return base.OnExposeEvent(args);
			}
		}
		
		[GtkBeans.Builder.Object] Gtk.TreeView themes_tree;
		[GtkBeans.Builder.Object] Gtk.HPaned hpaned;
		[GtkBeans.Builder.Object] Gtk.VBox backpreview_box;
		[GtkBeans.Builder.Object] Gtk.HBox file_hbox;
		[GtkBeans.Builder.Object] Gtk.TextView textview_intro;
		[GtkBeans.Builder.Object] Gtk.Label name_label;
		[GtkBeans.Builder.Object] Gtk.Label author_label;
		[GtkBeans.Builder.Object] Gtk.HBox selectbutton_box;
		[GtkBeans.Builder.Object] Gtk.HBox highlightbutton_box;

		ListStore store;
		Project project;
		Theme selected_theme;
		Gdk.Color color;
		BrowseFile browse_file;
		MenuBackgroundPreview menu_drawing_area;
		ButtonPreview highlightbutton_drawing_area;
		ButtonPreview selectbutton_drawing_area;

		const int COL_DISPLAY_NAME = 0;
		const int COL_OBJECT = 1;

		public ThemeSelectionDialog (Project project) : base ("ThemeSelectionDialog.ui", "themeselection")
		{
			this.project = project;
			hpaned.Position = 200; // W (left)
			selected_theme = null;
			menu_drawing_area = new MenuBackgroundPreview ();
			selectbutton_drawing_area = new ButtonPreview (project);
			highlightbutton_drawing_area = new ButtonPreview (project);

			browse_file = new BrowseFile (file_hbox, project.Details.CustomMenuBackground, true);
			browse_file.DefaultDirectory = Mistelix.Preferences.GetStringValue (Preferences.ImagesDirectoryKey);
			menu_drawing_area.CustomMenuBackground = project.Details.CustomMenuBackground;

			color = author_label.Style.Background (StateType.Normal);
			textview_intro.ModifyBase (Gtk.StateType.Normal, color);
	
			Gtk.Button clean_button = new Gtk.Button (Catalog.GetString ("Clean"));
			clean_button.Clicked += delegate 
			{
				browse_file.Filename = null;
				menu_drawing_area.CustomMenuBackground = null; 
			};

			file_hbox.Add (clean_button);
			file_hbox.ShowAll ();
			
			PrepareTree ();
			backpreview_box.Add (menu_drawing_area);
			backpreview_box.ShowAll ();

			highlightbutton_box.Add (highlightbutton_drawing_area);
			highlightbutton_box.ShowAll ();

			selectbutton_box.Add (selectbutton_drawing_area);
			selectbutton_box.ShowAll ();

			browse_file.FileSelectedChanged += delegate 
			{
				menu_drawing_area.CustomMenuBackground = browse_file.Filename;
			};

			Gtk.TextBuffer buffer_intro = new Gtk.TextBuffer (new Gtk.TextTagTable ());
			textview_intro.Buffer = buffer_intro;
			buffer_intro.Text = String.Format (
				Catalog.GetString ("Menu themes define how the main DVD menu looks. Here you define the default background image and the type of buttons. For more information visit: {0}"),
				Defines.THEMES_URL);
		}

		void PrepareTree ()
		{
			themes_tree.AppendColumn (Catalog.GetString ("Name"), new CellRendererText (), "text", COL_DISPLAY_NAME);
			// Name, object
			themes_tree.Model = store = new ListStore (typeof (string), typeof (Theme));

			TreePath path;
			TreeIter iter;
			
			foreach (Theme theme in ThemeManager.List)
				store.AppendValues (theme.Name, theme);
				
			store.GetIterFirst (out iter);
			path = store.GetPath (iter);

			themes_tree.CursorChanged += OnCursorChanged;
			themes_tree.SetCursor (path, themes_tree.Columns[0], false);
		}

		void OnOK (object sender, EventArgs args)
		{
			Logger.Debug ("ThemeSelectionDialog.OnOK");
			project.Details.ThemeName = selected_theme.Name;
			project.Details.CustomMenuBackground = browse_file.Filename;
		}

		void OnCursorChanged (object obj,  EventArgs e)
		{
			TreeModel model;
			TreeIter iter;
			TreeSelection selection = (obj as TreeView).Selection;

			if (selection.GetSelected (out model, out iter) == false)
				return;

			selected_theme = menu_drawing_area.Theme = (Theme) model.GetValue (iter, COL_OBJECT);
			selectbutton_drawing_area.ImageFile = System.IO.Path.Combine (Defines.DATA_DIR, selected_theme.ButtonSelect);
			highlightbutton_drawing_area.ImageFile = System.IO.Path.Combine (Defines.DATA_DIR, selected_theme.ButtonHighlight);
	
			name_label.Text = selected_theme.Name;
			author_label.Text = selected_theme.Author;

			Logger.Debug ("ThemeSelectionDialog. Selected theme {0}", selected_theme.Name);
		}
	}
}
